# ndaal_public_hashtables_circle.lu_normalized_code



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ndaal_open_source/ndaal_public_hashtables_circle.lu_normalized_code.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/ndaal_open_source/ndaal_public_hashtables_circle.lu_normalized_code/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.


## Scorecard

Aggregate score: 9.8 / 10

Check scores:


| SCORE   | NAME             | REASON                                                           | DETAILS | DOCUMENTATION/REMEDIATION                                                                                         |
|---------|------------------|------------------------------------------------------------------|---------|-------------------------------------------------------------------------------------------------------------------|
| 10 / 10 | Binary-Artifacts | no binaries found in the repo                                    |         | <https://github.com/ossf/scorecard/blob/49c0eed3a423f00c872b5c3c9f1bbca9e8aae799/docs/checks.md#binary-artifacts> |
| 9 / 10  | Code-Review      | found 1 unreviewed changesets out of 17 -- score normalized to 9 |         | <https://github.com/ossf/scorecard/blob/49c0eed3a423f00c872b5c3c9f1bbca9e8aae799/docs/checks.md#code-review>      |
| |   | 9 |   | / |   | 1 | 0 |   | | |   | C | o | d | e | - | R | e | v | i | e | w |   | | |   | f | o | u | n | d |   | 1 |   | u | n | r | e | v | i | e | w | e | d |   | c | h | a | n | g | e | s | e | t | s |   | o | u | t |   | o | f |   | 1 | 7 |   | - | - |   | s | c | o | r | e |   | n | o | r | m | a | l | i | z | e | d |   | t | o |   | 9 |   | | |   |   | | |   | h | t | t | p | s | : | / | / | g | i | t | h | u | b | . | c | o | m | / | o | s | s | f | / | s | c | o | r | e | c | a | r | d | / | b | l | o | b | / | 4 | 9 | c | 0 | e | e | d | 3 | a | 4 | 2 | 3 | f | 0 | 0 | c | 8 | 7 | 2 | b | 5 | c | 3 | c | 9 | f | 1 | b | b | c | a | 9 | e | 8 | a | a | e | 7 | 9 | 9 | / | d | o | c | s | / | c | h | e | c | k | s | . | m | d | # | c | o | d | e | - | r | e | v | i | e | w |   | |
| |   | | |   |   |   | | |   | 9 |   | | |   |   |   | | |   | / |   | | |   |   |   | | |   | 1 |   | | |   | 0 |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | C |   | | |   | o |   | | |   | d |   | | |   | e |   | | |   | - |   | | |   | R |   | | |   | e |   | | |   | v |   | | |   | i |   | | |   | e |   | | |   | w |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | f |   | | |   | o |   | | |   | u |   | | |   | n |   | | |   | d |   | | |   |   |   | | |   | 1 |   | | |   |   |   | | |   | u |   | | |   | n |   | | |   | r |   | | |   | e |   | | |   | v |   | | |   | i |   | | |   | e |   | | |   | w |   | | |   | e |   | | |   | d |   | | |   |   |   | | |   | c |   | | |   | h |   | | |   | a |   | | |   | n |   | | |   | g |   | | |   | e |   | | |   | s |   | | |   | e |   | | |   | t |   | | |   | s |   | | |   |   |   | | |   | o |   | | |   | u |   | | |   | t |   | | |   |   |   | | |   | o |   | | |   | f |   | | |   |   |   | | |   | 1 |   | | |   | 7 |   | | |   |   |   | | |   | - |   | | |   | - |   | | |   |   |   | | |   | s |   | | |   | c |   | | |   | o |   | | |   | r |   | | |   | e |   | | |   |   |   | | |   | n |   | | |   | o |   | | |   | r |   | | |   | m |   | | |   | a |   | | |   | l |   | | |   | i |   | | |   | z |   | | |   | e |   | | |   | d |   | | |   |   |   | | |   | t |   | | |   | o |   | | |   |   |   | | |   | 9 |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | h |   | | |   | t |   | | |   | t |   | | |   | p |   | | |   | s |   | | |   | : |   | | |   | / |   | | |   | / |   | | |   | g |   | | |   | i |   | | |   | t |   | | |   | h |   | | |   | u |   | | |   | b |   | | |   | . |   | | |   | c |   | | |   | o |   | | |   | m |   | | |   | / |   | | |   | o |   | | |   | s |   | | |   | s |   | | |   | f |   | | |   | / |   | | |   | s |   | | |   | c |   | | |   | o |   | | |   | r |   | | |   | e |   | | |   | c |   | | |   | a |   | | |   | r |   | | |   | d |   | | |   | / |   | | |   | b |   | | |   | l |   | | |   | o |   | | |   | b |   | | |   | / |   | | |   | 4 |   | | |   | 9 |   | | |   | c |   | | |   | 0 |   | | |   | e |   | | |   | e |   | | |   | d |   | | |   | 3 |   | | |   | a |   | | |   | 4 |   | | |   | 2 |   | | |   | 3 |   | | |   | f |   | | |   | 0 |   | | |   | 0 |   | | |   | c |   | | |   | 8 |   | | |   | 7 |   | | |   | 2 |   | | |   | b |   | | |   | 5 |   | | |   | c |   | | |   | 3 |   | | |   | c |   | | |   | 9 |   | | |   | f |   | | |   | 1 |   | | |   | b |   | | |   | b |   | | |   | c |   | | |   | a |   | | |   | 9 |   | | |   | e |   | | |   | 8 |   | | |   | a |   | | |   | a |   | | |   | e |   | | |   | 7 |   | | |   | 9 |   | | |   | 9 |   | | |   | / |   | | |   | d |   | | |   | o |   | | |   | c |   | | |   | s |   | | |   | / |   | | |   | c |   | | |   | h |   | | |   | e |   | | |   | c |   | | |   | k |   | | |   | s |   | | |   | . |   | | |   | m |   | | |   | d |   | | |   | # |   | | |   | c |   | | |   | o |   | | |   | d |   | | |   | e |   | | |   | - |   | | |   | r |   | | |   | e |   | | |   | v |   | | |   | i |   | | |   | e |   | | |   | w |   | | |   |   |   | | |   | |
| 10 / 10 | License | license file detected | Info: License file found in expected location: LICENSE:1 Info: FSF or OSI recognized license: LICENSE:1 | <https://github.com/ossf/scorecard/blob/49c0eed3a423f00c872b5c3c9f1bbca9e8aae799/docs/checks.md#license> |
| |   | 1 | 0 |   | / |   | 1 | 0 |   | | |   | L | i | c | e | n | s | e |   | | |   | l | i | c | e | n | s | e |   | f | i | l | e |   | d | e | t | e | c | t | e | d |   | | |   | I | n | f | o | : |   | L | i | c | e | n | s | e |   | f | i | l | e |   | f | o | u | n | d |   | i | n |   | e | x | p | e | c | t | e | d |   | l | o | c | a | t | i | o | n | : |   | L | I | C | E | N | S | E | : | 1 |   | I | n | f | o | : |   | F | S | F |   | o | r |   | O | S | I |   | r | e | c | o | g | n | i | z | e | d |   | l | i | c | e | n | s | e | : |   | L | I | C | E | N | S | E | : | 1 |   | | |   | h | t | t | p | s | : | / | / | g | i | t | h | u | b | . | c | o | m | / | o | s | s | f | / | s | c | o | r | e | c | a | r | d | / | b | l | o | b | / | 4 | 9 | c | 0 | e | e | d | 3 | a | 4 | 2 | 3 | f | 0 | 0 | c | 8 | 7 | 2 | b | 5 | c | 3 | c | 9 | f | 1 | b | b | c | a | 9 | e | 8 | a | a | e | 7 | 9 | 9 | / | d | o | c | s | / | c | h | e | c | k | s | . | m | d | # | l | i | c | e | n | s | e |   | |
| |   | | |   |   |   | | |   | 1 |   | | |   | 0 |   | | |   |   |   | | |   | / |   | | |   |   |   | | |   | 1 |   | | |   | 0 |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | L |   | | |   | i |   | | |   | c |   | | |   | e |   | | |   | n |   | | |   | s |   | | |   | e |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | l |   | | |   | i |   | | |   | c |   | | |   | e |   | | |   | n |   | | |   | s |   | | |   | e |   | | |   |   |   | | |   | f |   | | |   | i |   | | |   | l |   | | |   | e |   | | |   |   |   | | |   | d |   | | |   | e |   | | |   | t |   | | |   | e |   | | |   | c |   | | |   | t |   | | |   | e |   | | |   | d |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | I |   | | |   | n |   | | |   | f |   | | |   | o |   | | |   | : |   | | |   |   |   | | |   | L |   | | |   | i |   | | |   | c |   | | |   | e |   | | |   | n |   | | |   | s |   | | |   | e |   | | |   |   |   | | |   | f |   | | |   | i |   | | |   | l |   | | |   | e |   | | |   |   |   | | |   | f |   | | |   | o |   | | |   | u |   | | |   | n |   | | |   | d |   | | |   |   |   | | |   | i |   | | |   | n |   | | |   |   |   | | |   | e |   | | |   | x |   | | |   | p |   | | |   | e |   | | |   | c |   | | |   | t |   | | |   | e |   | | |   | d |   | | |   |   |   | | |   | l |   | | |   | o |   | | |   | c |   | | |   | a |   | | |   | t |   | | |   | i |   | | |   | o |   | | |   | n |   | | |   | : |   | | |   |   |   | | |   | L |   | | |   | I |   | | |   | C |   | | |   | E |   | | |   | N |   | | |   | S |   | | |   | E |   | | |   | : |   | | |   | 1 |   | | |   |   |   | | |   | I |   | | |   | n |   | | |   | f |   | | |   | o |   | | |   | : |   | | |   |   |   | | |   | F |   | | |   | S |   | | |   | F |   | | |   |   |   | | |   | o |   | | |   | r |   | | |   |   |   | | |   | O |   | | |   | S |   | | |   | I |   | | |   |   |   | | |   | r |   | | |   | e |   | | |   | c |   | | |   | o |   | | |   | g |   | | |   | n |   | | |   | i |   | | |   | z |   | | |   | e |   | | |   | d |   | | |   |   |   | | |   | l |   | | |   | i |   | | |   | c |   | | |   | e |   | | |   | n |   | | |   | s |   | | |   | e |   | | |   | : |   | | |   |   |   | | |   | L |   | | |   | I |   | | |   | C |   | | |   | E |   | | |   | N |   | | |   | S |   | | |   | E |   | | |   | : |   | | |   | 1 |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | h |   | | |   | t |   | | |   | t |   | | |   | p |   | | |   | s |   | | |   | : |   | | |   | / |   | | |   | / |   | | |   | g |   | | |   | i |   | | |   | t |   | | |   | h |   | | |   | u |   | | |   | b |   | | |   | . |   | | |   | c |   | | |   | o |   | | |   | m |   | | |   | / |   | | |   | o |   | | |   | s |   | | |   | s |   | | |   | f |   | | |   | / |   | | |   | s |   | | |   | c |   | | |   | o |   | | |   | r |   | | |   | e |   | | |   | c |   | | |   | a |   | | |   | r |   | | |   | d |   | | |   | / |   | | |   | b |   | | |   | l |   | | |   | o |   | | |   | b |   | | |   | / |   | | |   | 4 |   | | |   | 9 |   | | |   | c |   | | |   | 0 |   | | |   | e |   | | |   | e |   | | |   | d |   | | |   | 3 |   | | |   | a |   | | |   | 4 |   | | |   | 2 |   | | |   | 3 |   | | |   | f |   | | |   | 0 |   | | |   | 0 |   | | |   | c |   | | |   | 8 |   | | |   | 7 |   | | |   | 2 |   | | |   | b |   | | |   | 5 |   | | |   | c |   | | |   | 3 |   | | |   | c |   | | |   | 9 |   | | |   | f |   | | |   | 1 |   | | |   | b |   | | |   | b |   | | |   | c |   | | |   | a |   | | |   | 9 |   | | |   | e |   | | |   | 8 |   | | |   | a |   | | |   | a |   | | |   | e |   | | |   | 7 |   | | |   | 9 |   | | |   | 9 |   | | |   | / |   | | |   | d |   | | |   | o |   | | |   | c |   | | |   | s |   | | |   | / |   | | |   | c |   | | |   | h |   | | |   | e |   | | |   | c |   | | |   | k |   | | |   | s |   | | |   | . |   | | |   | m |   | | |   | d |   | | |   | # |   | | |   | l |   | | |   | i |   | | |   | c |   | | |   | e |   | | |   | n |   | | |   | s |   | | |   | e |   | | |   |   |   | | |   | |
| 10 / 10 | Security-Policy | security policy file detected | Info: security policy file detected: SECURITY.md:1 Info: Found linked content: SECURITY.md:1 Info: Found disclosure, vulnerability, and/or timelines in security policy: SECURITY.md:1 Info: Found text in security policy: SECURITY.md:1 | <https://github.com/ossf/scorecard/blob/49c0eed3a423f00c872b5c3c9f1bbca9e8aae799/docs/checks.md#security-policy> |
| 10 / 10 | Vulnerabilities | no vulnerabilities detected |  | <https://github.com/ossf/scorecard/blob/49c0eed3a423f00c872b5c3c9f1bbca9e8aae799/docs/checks.md#vulnerabilities> |

## Folder Structure
The Folder Structure consist at least of the following components in our project:

### Basic Folder Structure
The Basic Folder Structure consist at least of the following folders:

#### src Folder
The source code folder! However, in languages that use headers (or if you have a framework for your application) don’t put those files in here.

#### test Folder
Unit tests, integration tests… go here.

#### .config Folder
It should local configuration related to setup on local machine.

#### .build Folder
This folder should contain all scripts related to build process (PowerShell, Docker compose…).

#### dep Folder
This is the directory where all your dependencies should be stored.

#### documentation or doc Folder
The documentation folder

#### repo_check_results Folder
The repo check results folder

#### res Folder
For all static resources in your project. For example, images.

#### samples Folder
Providing “Hello World” & Co code that supports the documentation.

#### tools Folder
Convenience directory for your use. Should contain scripts to automate tasks in the project, for example, build scripts, rename scripts. Usually contains .sh, .cmd files for example.

### Repo folder structure
Git Special Files go here

#### .gitignore File
List of blobs for git to ignore. Affects commands like git add and git clean. You may use gitignore.io to generate a clean and useful gitignore file.
see also <https://gitlab.com/ndaal_open_source/ndaal_public_git_ignore>

#### .gitattributes
Let’s you define attributes on files (e.g., to change how files look in a diff).

#### .mailmap
Lets you tell git that duplicate names or emails in the history are actually the same person.

#### .gitmodules
Let’s you define submodules (subdirectories of your git repository which are checkouts of other git repositories).

#### .semgrepignore File
The .semgrepignore File describes two types of ignore operations for semgrep:

- Ignoring as exclusion. Exclude or skip specific files and folders from
  the scope of Semgrep scans in your repository or working directory.
  Ignoring in this context means that Semgrep does not generate findings
  for the ignored files and folders.
- Ignoring as triage action. Ignore specific parts of code that would have
  generated a finding. Ignoring in this context means that Semgrep generates
  a finding record and automatically triages it as Ignored, a triage state.

At least the following define ignored files and folders rules are in place
and active:

```
## Common large paths
node_modules/
build/
dist/
vendor/
.env/
.venv/
.tox/
*.min.js
.npm/
.yarn/

## Common test paths
test/
tests/
*_test.go

## Semgrep rules folder
.semgrep

## Semgrep-action log folder
.semgrep_logs/

## paths 

## files
.gitleaks.toml
.gitleaksignore
.gitignore 
```

#### .ansible-lint File
Ansible Lint is a command-line tool for linting playbooks, roles and collections aimed toward any Ansible users. Its main goal is to promote proven practices, patterns and behaviors while avoiding common pitfalls that can easily lead to bugs or make code harder to maintain.

At least our .ansible-lint File has the following content:

```
---
##
exclude_paths:
  - ./meta/preferences.yml
  - ./molecule/default/prepare.yml
  - ./molecule/default/converge.yml
  - ./molecule/default/verify.yml
  - ./molecule/default/collections.yml
  - ./.tox
  - ./.cache
```

- <https://github.com/ansible/ansible-lint>

## Ansible managed
#### .shellcheckrc File
ShellCheck is a static analysis and linting tool for sh/bash scripts. It's mainly focused on handling typical beginner and intermediate level syntax errors and pitfalls where the shell just gives a cryptic error message or strange behavior, but it also reports on a few more advanced issues where corner cases can cause delayed failures.

Unless --norc is used, ShellCheck will look for a file .shellcheckrc or shellcheckrc in the script's directory and each parent directory. If found, it will read key=value pairs from it and treat them as file-wide directives.

Here is an example .shellcheckrc:

```
## Look for 'source'd files relative to the checked script,
## and also look for absolute paths in /mnt/chroot
source-path=SCRIPTDIR
source-path=/mnt/chroot

## Since 0.9.0, values can be quoted with '' or "" to allow spaces
source-path="My Documents/scripts"

## Allow opening any 'source'd file, even if not specified as input
external-sources=true

## Turn on warnings for unquoted variables with safe values
enable=quote-safe-variables

## Turn on warnings for unassigned uppercase variables
enable=check-unassigned-uppercase

## Allow [ ! -z foo ] instead of suggesting -n
disable=SC2236
```

- <https://github.com/koalaman/shellcheck>

#### pyproject.toml File
pyproject.toml file is a configuration file used by packaging tools, as well as other tools such as linters, type checkers, etc. There are three possible TOML tables in this file.

- The [build-system] table is strongly recommended. It allows you to declare which build backend you use and which other dependencies are needed to build your project.

- The [project] table is the format that most build backends use to specify your project’s basic metadata, such as the dependencies, your name, etc.

- The [tool] table has tool-specific subtables, e.g., ```tool.hatch```, ```tool.black```, ```tool.mypy```. We only touch upon this table here because its contents are defined by each tool. Consult the particular tool’s documentation to know what it can contain.

- <https://packaging.python.org/en/latest/guides/writing-pyproject-toml/>

### Git Special Files & Folders

#### README File
README or README.txt or README.md or README.rst etc. is a file that answer the What, Why and How of the project. Git like GitLab or GitHub will recognize and automatically surface the README to repository visitors. Here is an awesome list for more professional readme files.

#### LICENSE File
LICENSE or LICENSE.txt or LICENSE.md etc. is a file that explains the legal licensing, such as any rights, any restrictions, any regulations, etc.

#### CHANGELOG File
CHANGELOG or CHANGELOG.txt or CHANGELOG.md or CHANGELOG.rst etc. is a file that describes what's happening in the repo. Version number increases, software updates, bug fixes… are examples of the file’s content.

#### CONTRIBUTORS File
CONTRIBUTORS or CONTRIBUTORS.txt or CONTRIBUTORS.md etc. is a file that lists people who have contributed to the repo.

#### AUTHORS File
AUTHORS or AUTHORS.txt or AUTHORS.md etc. is a file that lists people who are significant authors of the project, such as the people who are legally related to the work.

#### SUPPORT File
SUPPORT or SUPPORT.txt or SUPPORT.md etc. is a file that explains how a reader can get help with the repository. GitHub links this file on the page "New Issue".

#### SECURITY File
SECURITY (represented by security.txt and available here: <https://ndaal.eu/.well-known/security.txt>) describes our project's security policies, including a list of versions that are currently being maintained with security updates. It also gives instructions on how your users can submit a report of a vulnerability. Fore more details, check the following link.

#### Vulnerability Disclosure Policy File
Vulnerability Disclosure Policy (represented by Vulnerability_Disclosure_Policy.md and available here: <https://gitlab.com/vPierre/ndaal_public_vulnerability_disclosure_policy>) describes our project's Vulnerability Disclosure Policy.

#### CODE_OF_CONDUCT File
CODE_OF_CONDUCT is a file that explains how to engage in a community and how to address any problems among members of your project's community.

Our CODE_OF_CONDUCT based on Contributor Covenant 2.1.

#### CONTRIBUTING File
CONTRIBUTING is a file that explains how people should contribute, and that can help verify people are submitting well-formed pull requests and opening useful issues. GitHub links this file on page "New Issue" and the page "New Pull Request". This helps people understand how to contribute.

#### ACKNOWLEDGMENTS File
ACKNOWLEDGMENTS or ACKNOWLEDGMENTS.txt or ACKNOWLEDGMENTS.md etc. is a file that describes related work, such as other projects that are dependencies, or libraries, or modules, or have their own copyrights or licenses that you want to include in your project.

#### CODEOWNERS File
CODEOWNERS is a file that defines individuals or teams that are responsible for code in a repository. Code owners are automatically requested for review when someone opens a pull request that modifies code that they own. When someone with admin or owner permissions has enabled required reviews, they also can optionally require approval from a code owner before the author can merge a pull request in the repository.

#### FUNDING File
funding.yml is a file to raise funding for or support your project.

#### ISSUE_TEMPLATE File
When you add an issue template to your repository, project contributors will automatically see the template’s contents in the issue body. Templates customize and standardize the information you’d like included when contributors open issues. To add multiple issue templates to a repository create an ISSUE_TEMPLATE/ directory in your project root. Within that ISSUE_TEMPLATE/ directory you can create as many issue templates as you need, for example ISSUE_TEMPLATE/bugs.md. This list contains multiple templates for issues and pull requests.

#### PULL_REQUEST_TEMPLATE File
When you add a PULL_REQUEST_TEMPLATE file to your repository, project contributors will automatically see the template's contents in the pull request body. Templates customize and standardize the information you'd like included when contributors create pull requests. You can create a PULL_REQUEST_TEMPLATE/ subdirectory in any of the supported folders to contain multiple pull request templates.

### Other File(s)
Special Files like the Software Bill of Materials (SBOM) or crawling and indexing information based on robots.txt go here

#### SBOM File
sbom.json  is a file for Software Bill of Materials (SBOM). A Software Bill of Materials (SBOM) is a complete, formally structured list of components, libraries, and modules that are required to build (i.e. compile and link) a given piece of software and the supply chain relationships between them.

- <https://www.linuxfoundation.org/blog/blog/what-is-an-sbom>
- <https://www.csoonline.com/article/573185/what-is-an-sbom-software-bill-of-materials-explained.html>
- <https://www.nist.gov/itl/executive-order-14028-improving-nations-cybersecurity/software-security-supply-chains-software-1>

#### humans.txt File
What is humans.txt?
It's an initiative for knowing the people behind a website. It's a TXT file that contains information about the different people who have contributed to building the website.
Where is it located?
In the site root. Just next to the robots.txt file.
Why should I?
You don't have to if you don't want. The only aim of this initiative is to know who the authors of the sites we visit are.
Meta Name or humans.txt?
This is not a fight, you don't have to choose one or the other. Humans.txt is just a way to have more information about the authors of the site.
The internet is for humans...
We are always saying that, but the only file we generate is one full of additional information for the searchbots: robots.txt. Then why not doing one for ourselves?
- <https://humanstxt.org/>

#### robots.txt File
The robots.txt file is to prevent the crawling and indexing of certain parts
of your site by web crawlers and spiders run by sites like Yahoo!
and Google. By telling these "robots" where not to go on your site,
you save bandwidth and server resources.

- <https://www.searchenginejournal.com/robots-txt-security-risks/289719/>

Security wise, robots.txt usage has two rules.

Do not try to implement any security through robots.txt. The robots file is
nothing more than a kind suggestion, and while most search engine crawlers
respect it, malicious crawlers have a good laugh and continue at their
business. If it's linked to, it can be found.

Do not expose interesting information through robots.txt. Specifically,
if you rely on the URL to control access to certain resources (which is a
huge alarm bell by itself), adding it to robots.txt will only make the
problem worse: an attacker who scans robots.txt will now see the secret URL
you were trying to hide, and concentrate efforts on that part of your site
(you don't want it indexed, and it's named
'sekrit-admin-part-do-not-tell-anyone', so it's probably interesting).

#### structure.txt File
output a tree of the repo

#### .gitleaksignore File
You can ignore specific findings by creating a .gitleaksignore file at the root of your repo. In release v8.10.0 Gitleaks added a Fingerprint value to the Gitleaks report. Each leak, or finding, has a Fingerprint that uniquely identifies a secret. Add this fingerprint to the .gitleaksignore file to ignore that specific secret.

#### content_summary.txt File

content_summary.txt File is created by the rust program tokei
in our CI/CD pipeline.
Tokei is a program that displays statistics about your code. Tokei will show
the number of files, total lines within those files and code, comments, and
blanks grouped by language.

This File represents a summary of these information.

#### content_long_list.txt File
content_long_list.txt File is created by the rust program tokei
by our CI/CD pipeline.
Tokei is a program that displays statistics about your code. Tokei will show
the number of files, total lines within those files and code, comments, and
blanks grouped by language.

This File represents a detailed overview of these information.

#### scorecard File
Scorecard assesses open source projects for security risks through a series
of automated checks. It was created by OSS developers to help improve
the health of critical projects that the community depends on.

You can use it to proactively assess and make informed decisions about
accepting security risks within your codebase. You can also use the tool
to evaluate other projects and dependencies, and work with maintainers
to improve codebases you might want to integrate.

- <https://securityscorecards.dev/>

We put the scorecard File scorecard.md in /documentation/scorecard/

#### checksum File(s)
Checksum files, often accompanied by their cryptographic collision-resistant
hash values, serve the purpose of ensuring the integrity of artifacts
within a public Git repository. These hash values act as unique fingerprints
for files, generated using algorithms like SHA-256, SHA-512, or others.
By providing checksum files alongside artifacts, contributors and
visitors can:

##### Verify Data Integrity
Checksums act as a digital signature for files, ensuring that the content
hasn't been altered or corrupted.

##### Prevent Tampering
Detect any unauthorized modifications to files, preventing malicious tampering
or unintentional corruption.

##### Ensure Authenticity
Confirm that the artifacts were indeed produced by the expected source
and have not been replaced with malicious versions.

##### Facilitate Secure Downloads
Users can compare the calculated checksum of downloaded files
with the provided checksums to ensure they received
an unmodified copy.

##### Maintain Data Consistency
Provide a reliable mechanism for maintaining consistency
across distributed repositories and diverse contributors.

In summary, checksum files and their associated hash values play
a crucial role in maintaining the trustworthiness and reliability
of artifacts in a public Git repository, fostering a secure and
collaborative development environment.

In our approach, we construct the names of checksum files based
on the original artifact file name and the hash algorithm used.
For example:

For an artifact file named "alpine-standard-3.19.0-aarch64.iso"
using the blake2b hash algorithm, the corresponding checksum file name
would be "alpine-standard-3.19.0-aarch64.iso.blake2b".

Similarly, for SHA-256, the checksum file name would be
"alpine-standard-3.19.0-aarch64.iso.sha256".

For k12, the checksum file name would be
"alpine-standard-3.19.0-aarch64.iso.k12".

This naming convention helps contributors and visitors quickly identify
the checksum files associated with each artifact, streamlining the process
of verifying the integrity of files in the repository. The use
of different hash algorithms provides flexibility and allows users
to choose the level of cryptographic strength based
on their security and compliance requirements.

### Hints
If you see only a placeholder.txt File in a folder you can ignore
this directory. It was automatically created by our CI/CD pipeline.

#### .gitkeep .placeholder placeholder.txt approach File(s)
Use an empty file called .gitkeep .placeholder placeholder.txt in order
to force the presence of the folder in the versioning system.

Although it may seem not such a big difference:

- You use a file that has the single purpose of keeping the folder.
  You don't put there any info you don't want to put.

  For instance, you should use READMEs as, well, READMEs with useful
  information, not as an excuse to keep the folder.

  Separation of concerns is always a good thing, and you can still add a
  .gitignore to ignore unwanted files.

- Naming it .gitkeep, .placeholder, placeholder.txt makes it very clear
  and straightforward from the filename itself (and also to other developers,
  which is good for a shared project and one of the core purposes of a
  Git repository) that this file is

  - A file unrelated to the code (because of the leading dot and the name)
  - A file clearly related to Git
  - Its purpose (keep) is clearly stated and consistent and
    semantically opposed in its meaning to ignore
